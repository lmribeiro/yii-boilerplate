<html>
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <style>
            a {
                color: #39CCCC;
                text-decoration: none;
            }
            .footer {
                color: #616365;
            }
            .footer a {
                color: #616365;
            }
            a:hover {
                color: #00B6EC;
            }
        </style>
    </head>
    <body style="font-family: Verdana;">
        <table cellspacing="0" cellpadding="10" style="color:#0f0f0f; font:13px Verdana; line-height:1.4em; width:100%;">
            <tbody>
                <tr>
                    <td style="color:#0f0f0f; font-size:22px; border-bottom: 2px solid #cccccc; font-family: Verdana;">
                        <a href="<?= $this->createAbsoluteUrl('site/index') ?>">
                            <img alt="<?php echo CHtml::encode(Yii::app()->name); ?>" src="logo.png"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="color:#1f1f1f; font-size:18px; padding-top:20px; font-family: Verdana;">
                        <?php if (isset($data['description'])) echo $data['description']; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $content ?>
                    </td>
                </tr>
                <tr>
                    <td class="footer">
                        <table style="border-top:solid 2px #cccccc; font-family: Verdana;" cellpadding="0" cellspacing="0" width="100%" >
                            <tr style="padding-top: 10px; margin-top: 5px;"><td><p></p></td></tr>
                            <tr width="100%">
                                <td style="color: #000; font-size: 13px; font-family: Verdana;" width="75%">
                                    <p>
                                    <p style="margin-top: 2px; font-size: 11px; color: #616365;">&copy; <?= date("Y")." ".$data['entity'] ?> | Todos os direitos reservados.</p>
                                    </p>
                                </td>

                                <td align="right">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
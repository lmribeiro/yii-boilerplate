<?php

return CMap::mergeArray(
                require(dirname(__FILE__).'/main.php'), array(
            'preload' => array('log'),
            'components' => array(
                'db' => array(
                    'enableProfiling' => YII_DEBUG_PROFILING,
                    'enableParamLogging' => true,
                ),
                'fixture' => array(
                    'class' => 'system.test.CDbFixtureManager',
                ),
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                            'enabled' => YII_DEBUG_SHOW_PROFILER,
                            'categories' => 'system.db.*',
                            'ipFilters' => array('', '127.0.0.1', '192.168.1.215'),
                        ),
                    ),
                ),
            ),
                )
);

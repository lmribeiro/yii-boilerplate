<?php

class Crypt {

    const hash_algorithm = "sha256";
    const iterations = 1000;

    public static $salt_bytes = 32;
    public static $hash_bytes = 64;
    protected static $hash_sections = 4;
    protected static $hash_algorithm_index = 0;
    protected static $hash_iteration_index = 1;
    protected static $hash_salt_index = 2;
    protected static $hash_pbkdf2_index = 3;

    public static function createHash($password, &$salt = '') {
        if (!$salt) {

            if (function_exists('mcrypt_create_iv')) {
                $salt = substr(
                        base64_encode(mcrypt_create_iv(static::$salt_bytes, MCRYPT_DEV_URANDOM)), 0, static::$salt_bytes
                );
            }

            if ($salt === '' && is_readable('/dev/urandom') &&
                    ($hRand = @fopen('/dev/urandom', 'rb')) !== FALSE) {
                $salt = substr(
                        base64_encode(fread($hRand, static::$salt_bytes)), 0, static::$salt_bytes
                );
                fclose($hRand);
            }

            if ($salt === '') {
                $salt_bytes = static::$salt_bytes;
                $it = $salt_bytes % 23;
                $k = 0;

                while ($k <= $it) {
                    $salt .= uniqid("", true);
                    $k++;
                }

                $salt = substr($salt, 0, static::$salt_bytes);
            }
        }

        return substr(
                base64_encode(static::pbkdf2(
                                static::hash_algorithm, $password, $salt, static::iterations, static::$hash_bytes, true)
                ), 0, static::$hash_bytes
        );
    }

    public static function validatePassword($password, $good_hash, $salt) {
        $pbkdf2 = base64_decode($good_hash);

        return static::slowEquals(
                        $pbkdf2, static::pbkdf2(
                                static::hash_algorithm, $password, $salt, static::iterations, strlen($pbkdf2), true
                        )
        );
    }

    // Compares two strings $a and $b in length-constant time.
    protected static function slowEquals($a, $b) {
        $diff = strlen($a) ^ strlen($b);

        for ($i = 0; $i < strlen($a) && $i < strlen($b); $i++) {
            $diff |= ord($a[$i]) ^ ord($b[$i]);
        }

        return $diff === 0;
    }

    /*
     * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
     * $algorithm - The hash algorithm to use. Recommended: SHA256
     * $password - The password.
     * $salt - A salt that is unique to the password.
     * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
     * $key_length - The length of the derived key in bytes.
     * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
     * Returns: A $key_length-byte key derived from the password and salt.
     *
     * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
     *
     * This implementation of PBKDF2 was originally created by https://defuse.ca
     * With improvements by http://www.variations-of-shadow.com
     */

    public static function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {
        $algorithm = strtolower($algorithm);

        if (!in_array($algorithm, hash_algos(), true)) {
            throw new Exception('PBKDF2 ERROR: Invalid hash algorithm.');
        }

        if ($count <= 0 || $key_length <= 0) {
            throw new Exception('PBKDF2 ERROR: Invalid parameters.');
        }

        $hash_length = strlen(hash($algorithm, "", true));
        $block_count = ceil($key_length / $hash_length);

        $output = "";
        for ($i = 1; $i <= $block_count; $i++) {
            // $i encoded as 4 bytes, big endian.
            $last = $salt . pack("N", $i);
            // first iteration
            $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
            // perform the other $count - 1 iterations
            for ($j = 1; $j < $count; $j++) {
                $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
            }
            $output .= $xorsum;
        }

        if ($raw_output) {
            return substr($output, 0, $key_length);
        } else {
            return bin2hex(substr($output, 0, $key_length));
        }
    }

}

?>
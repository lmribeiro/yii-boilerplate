<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/stylish-portfolio.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css">

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 7]>
                    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
                <![endif]-->

        <!-- Side Menu -->
        <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle"><i class="fa fa-reorder"></i></a>
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
                <li class="sidebar-brand"><a href="#">Start Bootstrap</a></li>
                <li><a href="#top">Home</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <!-- /Side Menu -->

        <!-- Full Page Image Header Area -->
        <div id="top" class="header">
            <div class="vert-text">
                <h1>Start Bootstrap</h1>
                <h3><em>We</em> Build Great Templates, <em>You</em> Make Them Better</h3>
                <a href="#intro" class="btn btn-default btn-lg">Find Out More</a>
            </div>
        </div>
        <!-- /Full Page Image Header Area -->

        <!-- Intro -->
        <div id="intro" class="intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <h2>Subtle Sidebar is the Perfect Template for your Next Portfolio Website Project!</h2>
                        <p class="lead">This template really has it all. It's up to you to customize it to your liking! It features some fresh photography courtesy of <a target="_blank" href="http://join.deathtothestockphoto.com/">Death to the Stock Photo</a>.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Intro -->

        <!-- Services -->
        <div id="services" class="services">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <h2>Our Services</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-md-offset-2 text-center">
                        <div class="service-item">
                            <i class="service-icon fa fa-rocket"></i>
                            <h4>Spacecraft Repair</h4>
                            <p>Did your navigation system shut down in the middle of that asteroid field? We can repair any dings and scrapes to your spacecraft!</p>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="service-item">
                            <i class="service-icon fa fa-magnet"></i>
                            <h4>Problem Solving</h4>
                            <p>Need to know how magnets work? Our problem solving solutions team can help you identify problems and conduct exploratory research.</p>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="service-item">
                            <i class="service-icon fa fa-shield"></i>
                            <h4>Blacksmithing</h4>
                            <p>Planning a time travel trip to the middle ages? Preserve the space time continuum by blending in with period accurate armor and weapons.</p>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="service-item">
                            <i class="service-icon fa fa-pencil"></i>
                            <h4>Pencil Sharpening</h4>
                            <p>We've been voted the best pencil sharpening service for 10 consecutive years. If you have a pencil that feels dull, we'll get it sharp!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Services -->

        <!-- Callout -->
        <div class="callout">
            <div class="vert-text">
                <h1>A Dramatic Text Area</h1>
            </div>
        </div>
        <!-- /Callout -->

        <!-- Portfolio -->
        <div id ="portfolio" class="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <h2>Our Work</h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2 text-center">
                        <div class="portfolio-item">
                            <a href="#top"><img class="img-portfolio img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/img/portfolio-1.jpg"></a>
                            <h4>Cityscape</h4>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="portfolio-item">
                            <a href="#top"><img class="img-portfolio img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/img/portfolio-2.jpg"></a>
                            <h4>Is That On Fire?</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2 text-center">
                        <div class="portfolio-item">
                            <a href="#top"><img class="img-portfolio img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/img/portfolio-3.jpg"></a>
                            <h4>Stop Sign</h4>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="portfolio-item">
                            <a href="#top"><img class="img-portfolio img-responsive" src="<?= Yii::app()->theme->baseUrl; ?>/img/portfolio-4.jpg"></a>
                            <h4>Narrow Focus</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Portfolio -->

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <ul class="list-inline">
                            <li><i class="icon-facebook icon-2x"></i></li>
                            <li><i class="icon-twitter icon-2x"></i></li>
                            <li><i class="icon-dribbble icon-2x"></i></li>
                        </ul>
                        <hr>
                        <p>
                            Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
                            All Rights Reserved.<br/>
                            <?php echo Yii::powered(); ?>
                        </p>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- page -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function() {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X');
        ga('send', 'pageview');
    </script>
    <!-- Custom JavaScript for the Side Menu - Put in a custom JS file if you want to clean this up -->
    <script>
        $("#menu-close").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("active");
        });
    </script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("active");
        });
    </script>
    <script>
        $(function() {
            $('a[href*=#]:not([href=#])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        || location.hostname == this.hostname) {

                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>
</body>
</html>

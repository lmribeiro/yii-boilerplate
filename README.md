# Yii-Boilerplate #

### What is this repository for? ###

Yii-Boilerplate pretendes to be the starting point to quickly create a new project with Yii Framework.

### Versions ###
* 1.0.0 | 23/07/2014

### Permissions ###

find . -type f -print0 | xargs -0 chmod 644
find . -type d -print0 | xargs -0 chmod 755

chmod -R 777 assets protected/runtime

### Included extension ###

* AweCrud. (Fully configured)
* YiiMailer (Requires some configurations on /protected/config/mail.php)
* Bootstrap (Fully configured)
* Yii-debug-tollbar (Fully configured)

### Included CSS ###

* Font-awesome
* Normalize

### Included JS ###

* Jquery 1.10.2
* Modernizr 2.6.2

### Included JS Plugins ###

* Datatables (/private/vendor/datatables)
* Bootstrap Datatables - Fix for Bootstrap 3 (/private/vendor/bs-datatables)
* Messenger (/private/vendor/messenger)

### Included Themes ###

* Yii classic theme (default)
* Bootstrap One page theme (Just as an example)

### Changelog ###

* 16/01/2014 - Update Yii-debug-tollbar version 

### Contributes are welcome ###

Help improve this project and suggest new features or plugin.

### About ###
* Copyright © 2014 Código Irreverente, Lda.
* More information at [support@eddmi.com](mailto:support@eddmi.com)